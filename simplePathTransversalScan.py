import argparse
import os
import requests
from datetime import datetime
import csv
import time
'''
Script created by: pyDante
linkedin: pybalfaro
version 1.0
This script was developed for free use and only for ethical reasons, this is a simple pathtransversalscan
you can use in linux and windows but this was thought for windows use because i think the is not necesary to install a kali machine for this basic scan.

'''
class Spider:
    def __init__(self, file_path, base_url, interval):
        self.file_path = file_path
        self.base_url = base_url
        self.interval = interval

    def read_paths(self):
        try:
            with open(self.file_path, 'r') as file:
                paths = file.readlines()
                return [path.strip() for path in paths]
        except FileNotFoundError:
            print("Error: El archivo no se encontró.")
            return []

    def make_requests(self, paths):
        responses = {}
        for path in paths:
            url = f"{self.base_url}/{path}"
            response = requests.get(url)
            responses[path] = response.status_code
            print(f"Solicitud GET a {url}: Estado {response.status_code} - {response.reason} ")
            time.sleep(self.interval)
        return responses

    def save_status_codes_text(self, domain, responses):
        date_time = datetime.now().strftime("%m-%d-%Y-%I-%M-%p")
        file_name = f"{domain}-{date_time}.txt"
        with open(file_name, 'w') as file:
            file.write(f"Scan for: {self.base_url}\n")
            for path, status_code in responses.items():
                file.write(f"{path} - {status_code} - {reason}\n")
        return file_name

    def save_status_codes_csv(self, domain, responses):
        date_time = datetime.now().strftime("%m-%d-%Y-%I-%M-%p")
        file_name = f"{domain}-{date_time}.csv"
        with open(file_name, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["Path", "Status"])
            for path, status_code, reason in responses.items():
                writer.writerow([path, status_code])
        return file_name

def main():
    parser = argparse.ArgumentParser(description="Simple Directory Transversal Scan")
    parser.add_argument("-file", dest="file_path", required=True, help="Ruta del archivo con los paths")
    parser.add_argument("-url", dest="base_url", required=True, help="URL base")
    parser.add_argument("-interval", dest="interval", type=int, default=0, help="Intervalo entre solicitudes en segundos")
    args = parser.parse_args()

    spider = Spider(args.file_path, args.base_url, args.interval)
    paths = spider.read_paths()
    if paths:
        responses = spider.make_requests(paths)
        domain = args.base_url.split('//')[-1].split('/')[0]
        file_name_csv = spider.save_status_codes_csv(domain, responses)
        file_name_txt = spider.save_status_codes_text(domain, responses)
        print(f"Los códigos de estado se han guardado en el archivo {file_name_csv} (CSV) y {file_name_txt} (TXT)")

if __name__ == "__main__":
    main()